//
//  LoaderView.swift
//  projampa2
//
//  Created by Vinicius Albuquerque on 18/04/17.
//  Copyright © 2017 Vinicius Albuquerque. All rights reserved.
//

import UIKit
import SnapKit

public class LoadingOverlay{

var overlayView = UIView()
var activityIndicator = UIActivityIndicatorView()

class var shared: LoadingOverlay {
    struct Static {
        static let instance: LoadingOverlay = LoadingOverlay()
    }
    return Static.instance
}

public func showOverlay(view: UIView) {
    
    overlayView.frame = CGRect.init(x: 0, y: 0, width: 80, height: 80)
    overlayView.center = view.center
    overlayView.backgroundColor = UIColor(white: 0.5, alpha: 0.5)
    overlayView.clipsToBounds = true
    overlayView.layer.cornerRadius = 10
    
    activityIndicator.frame = CGRect.init(x: 0, y: 0, width: 40, height: 40)
    activityIndicator.activityIndicatorViewStyle = .whiteLarge
    activityIndicator.center = CGPoint.init(x: overlayView.bounds.width / 2, y: overlayView.bounds.height / 2)
    
    overlayView.addSubview(activityIndicator)
    view.addSubview(overlayView)
    
    activityIndicator.startAnimating()
}

public func hideOverlayView() {
    activityIndicator.stopAnimating()
    overlayView.removeFromSuperview()
}
}

