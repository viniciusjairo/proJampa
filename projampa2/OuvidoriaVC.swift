//
//  OuvidoriaVC.swift
//  projampa2
//
//  Created by Vinicius Albuquerque on 11/04/17.
//  Copyright © 2017 Vinicius Albuquerque. All rights reserved.
//

import UIKit
import MapKit

class OuvidoriaVC: UIViewController, CLLocationManagerDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var ruaField: UITextField!
    
    @IBOutlet weak var bairroField: UITextField!
    
    @IBOutlet weak var numeroField: UITextField!
    
    @IBOutlet weak var mensagemField: UITextView!
    
    var locationManager: CLLocationManager!
    var latitude: CLLocationDegrees = 0
    var longitude: CLLocationDegrees = 0
    
    @IBAction func enderecoBTN(_ sender: Any) {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        
        locationManager.startUpdatingLocation()
        
    }
    @IBAction func fotoBTN(_ sender: Any) {
        if(ruaField.text == "" || mensagemField.text == "" || bairroField.text == ""){
            let alertController = UIAlertController(title: "Erro", message: "Preencha os campos obrigatórios", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            }
            
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
            
            
            
        }else{
            let ouvidoria = Ouvidoria.init(rua: ruaField.text!,
                                           bairro: bairroField.text!,
                                           numero: numeroField.text!,
                                           mensagem: mensagemField.text!)
            
            performSegue(withIdentifier: "camera", sender: ouvidoria)
        }
    }
    
    
    @IBAction func limparBTN(_ sender: Any) {
        ruaField.text = ""
        bairroField.text = ""
        numeroField.text = ""
        mensagemField.text = ""
    }
    
    @IBAction func voltarBTN(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let image: UIImage = #imageLiteral(resourceName: "navigationBar")
        self.navigationController?.navigationBar.setBackgroundImage(image, for: .default)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(OuvidoriaVC.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissKeyboard(){
        //textField.resignFirstResponder()
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        ruaField.resignFirstResponder()
        bairroField.resignFirstResponder()
        mensagemField.resignFirstResponder()
        numeroField.resignFirstResponder()
        
        return true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location:CLLocationCoordinate2D = manager.location!.coordinate
        latitude = location.latitude
        longitude = location.longitude
        
        let Finallocation = CLLocation(latitude: latitude, longitude: longitude)
        CLGeocoder().reverseGeocodeLocation(Finallocation, completionHandler: {(placemarks, error) -> Void in
            print(Finallocation)
            
            if error != nil {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
            
            if placemarks!.count > 0 {
                let pm = placemarks![0]
                self.displayLocationInfo(placemark: pm)
            }
            else {
                print("Problem with the data received from geocoder")
            }
        })
        
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while updating location " + error.localizedDescription)
    }
    
    func displayLocationInfo(placemark: CLPlacemark) {
        //stop updating location to save battery life
        locationManager.stopUpdatingLocation()
        
        if((placemark.subLocality) != nil){
            bairroField.text = (placemark.subLocality)!
            
        }
        if((placemark.subThoroughfare) != nil){
            numeroField.text = (placemark.subThoroughfare)!
            
        }
        if((placemark.thoroughfare) != nil){
            ruaField.text = (placemark.thoroughfare)!
            
        }
        
    }
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "camera"){
            let cameraVC: CameraVC = segue.destination as! CameraVC
            let ouvidoria = sender as! Ouvidoria
            cameraVC.ouvidoria = ouvidoria
            
        }
    }
 
    
}
