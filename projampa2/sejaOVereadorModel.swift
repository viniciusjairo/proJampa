//
//  sejaOVereadorModel.swift
//  projampa2
//
//  Created by Vinicius Albuquerque on 20/04/17.
//  Copyright © 2017 Vinicius Albuquerque. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import SwiftyJSON

class SejaOVereadorModel: NSObject {
    var id: Int?
    var titulo: String?
    var conteudo: String?
    var img: String?
    
    init(id: Int,titulo: String, conteudo: String, img: String){
        self.id = id
        self.titulo = titulo
        self.conteudo = conteudo
        self.img = img
        
    }
    
    class func getDados(completionHandler: @escaping (_ SejaOVereador: SejaOVereadorModel) -> Void){
        let url = "sua url"
        
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default).responseJSON { response in
            let json = JSON(data: response.data!)
            let subJson = json[0]
            let SOV = SejaOVereadorModel.init(id: subJson["id"].intValue,
                                                             titulo: subJson["title"]["rendered"].stringValue,
                                                             conteudo: subJson["content"]["rendered"].stringValue,
                                                             img: subJson["better_featured_image"]["media_details"]["sizes"]["medium"]["source_url"].stringValue)
            completionHandler(SOV)
            
            
        }
        
        
        
        
    }
    
    class func sendDados(id: Int, titulo: String, votacao: String) {
        
        let url = "url para enviar os dados"
        
        let dataCorrente = Date()
        let formatter = DateFormatter()
        formatter.timeStyle = .medium
        formatter.dateStyle = .long
        //formatter.locale = NSLocale.autoupdatingCurrent
        let data = formatter.string(from: dataCorrente)
        
        
        let parametros = [
        "id":"\(id)",
        "titulo":titulo,
        "data":data,
        "votacao":votacao
        
        ]
        
        Alamofire.request(url, method: .post, parameters: parametros).responseJSON { response in
            
        }
        
    }
    
    
    
}
