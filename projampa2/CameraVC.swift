//
//  CameraVC.swift
//  projampa2
//
//  Created by Vinicius Albuquerque on 28/04/17.
//  Copyright © 2017 Vinicius Albuquerque. All rights reserved.
//

import UIKit

class CameraVC: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    var ouvidoria: Ouvidoria?
    let picker = UIImagePickerController()
    
    
    
    @IBOutlet weak var imageContainer: UIImageView!
    @IBAction func voltarBTN(_ sender: Any) {
        guard (navigationController?.popToRootViewController(animated: true)) != nil
            else {
                print("Algum coisa deu errado")
            return
        }
        
    }
    
    @IBAction func albumBTN(_ sender: Any) {
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
        
        
    }
    
    @IBOutlet weak var cameraImage: UIButton!
    
    @IBAction func cameraBTN(_ sender: Any) {
        picker.sourceType = .camera
        picker.allowsEditing = false

        self.present(picker, animated: true, completion: nil)
        
    }
    
    @IBAction func proximoBTN(_ sender: Any) {
        if(imageContainer.image == nil){
            ouvidoria?.img =  #imageLiteral(resourceName: "placeholder")
        }else {
            ouvidoria?.img = imageContainer.image!
        }
        self.ouvidoria?.salvarFoto() { (retorno, url) in
            if (retorno == true) {
                print(url)
                self.performSegue(withIdentifier: "enviar", sender: self.ouvidoria)

            }else {
                print("Algum erro aconteceu ao tentar enviar a foto")
            }
            
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        
        let image: UIImage = #imageLiteral(resourceName: "navigationBar")
        self.navigationController?.navigationBar.setBackgroundImage(image, for: .default)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        print(ouvidoria?.mensagem! ?? "testando")
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            imageContainer.image = chosenImage //4
            
        }else {
            print("Alguma coisa deu errado")
        }
        //imageContainer.contentMode = .scaleAspectFit //3
        self.dismiss(animated:true, completion: nil) //5
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "enviar"){
            let enviarVC: EnviarVC = segue.destination as! EnviarVC
            let ouvidoria = sender as! Ouvidoria
            enviarVC.ouvidoria = ouvidoria
            
        }
    }
    
    
}

