//
//  ViewController.swift
//  projampa2
//
//  Created by Vinicius Albuquerque on 23/03/17.
//  Copyright © 2017 Vinicius Albuquerque. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBAction func transparenciaBTN(_ sender: Any) {
        performSegue(withIdentifier: "transparencia", sender: self)
        
        
    }
    @IBAction func SejaOVereadorBTN(_ sender: Any) {
        performSegue(withIdentifier: "sejaVereador", sender: self)
    }
    
    
    @IBAction func ouvidoriaBTN(_ sender: Any) {
        performSegue(withIdentifier: "ouvidoria", sender: self)

        
    }
    
    @IBAction func colaboreBTN(_ sender: Any) {
        performSegue(withIdentifier: "colabore", sender: self)
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "sejaVereador"){
            let nav = segue.destination as! UINavigationController
            
            let SOVVC = nav.topViewController as! SejaOVereadorVC
            SejaOVereadorModel.getDados() { SOV in
                SOVVC.SOV = SOV
     
     }

     

     
        }
     
    }

    
    */

}

