//
//  ColaboreVC.swift
//  projampa2
//
//  Created by Vinicius Albuquerque on 11/04/17.
//  Copyright © 2017 Vinicius Albuquerque. All rights reserved.
//

import UIKit

class ColaboreVC: UIViewController, UIPickerViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var nomeField: UITextField!
    
    @IBOutlet weak var telefoneField: UITextField!
    
    @IBOutlet weak var emailField: UITextField!
    
    @IBOutlet weak var assuntoPV: UIPickerView!
    
    @IBOutlet weak var assuntoField: UITextView!
    
    @IBOutlet weak var aviso: UILabel!
    
    
    var pickerDados: [String] = [String]()
    
    var assunto: String  = "Saúde"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let image: UIImage = #imageLiteral(resourceName: "navigationBar")
        self.navigationController?.navigationBar.setBackgroundImage(image, for: .default)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        self.nomeField.delegate = self
        self.telefoneField.delegate = self
        self.emailField.delegate = self
        self.assuntoPV.delegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ColaboreVC.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        pickerDados = ["Saúde",
                       "Educação",
                       "Mobilidade Urbana",
                       "Esporte","Empreendedorismo",
                       "Turismo","Emprego e Renda",
                       "Infraestrutura","Meio Ambiente",
                       "Outros"]
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
    
    //TODO: terminar o enviar, validação de campos
    @IBAction func enviarBTN(_ sender: Any) {
        
        if (Conexao.isInternetAvailable() == false){
            let alertController = UIAlertController(title: "Erro", message: "Sem conexão com a internet", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                print("OK")
                self.dismiss(animated: true, completion: nil)
                
            }
            
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        if(self.nomeField.text == "" || self.emailField.text == "" || self.assuntoField.text == ""){
            
            shake()
            aviso.text = "Preencha os campos obrigatórios"
            return
            
        }
        let colabore = Colabore.init(nome: nomeField.text!,
                                     telefone: telefoneField.text!,
                                     email: emailField.text!,
                                     assunto: self.assunto,
                                     conteudo: assuntoField.text!)
        Colabore.enviarDados(colabore: colabore)
        let alertController = UIAlertController(title: "Sucesso", message: "Sua colaboração foi enviada com sucesso", preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            print("OK")
            self.dismiss(animated: true, completion: nil)
            
        }
        
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func voltarBTN(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func limparCampos(_ sender: Any) {
        self.nomeField.text = ""
        self.assuntoField.text = ""
        self.emailField.text = ""
        self.telefoneField.text = ""
        
        
    }
    
    
    func dismissKeyboard(){
        //textField.resignFirstResponder()
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.nomeField.resignFirstResponder()
        self.telefoneField.resignFirstResponder()
        self.emailField.resignFirstResponder()
        
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return pickerDados.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDados[row]
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        //arrumar quando for fazer a comunicação com o banco de dados
        self.assunto = pickerDados[row]
        
    }
    
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let color1 = UIColor.init(red: 28, green: 55, blue: 102, alpha: 1)
        
        let pickerLabel = UILabel()
        let titleData = pickerDados[row]
        let myTitle = NSAttributedString(string: titleData, attributes: [NSFontAttributeName:UIFont(name: "Arial-BoldMT", size: 17.0)!,NSForegroundColorAttributeName:color1])
        pickerLabel.attributedText = myTitle
        pickerLabel.textAlignment = .center
        return pickerLabel
    }
    
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        view.layer.add(animation, forKey: "shake")
    }
    
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
