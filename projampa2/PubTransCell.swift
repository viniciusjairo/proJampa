//
//  PubTransCell.swift
//  projampa2
//
//  Created by Vinicius Albuquerque on 18/04/17.
//  Copyright © 2017 Vinicius Albuquerque. All rights reserved.
//

import UIKit
import AlamofireImage

class PubTransCell: UITableViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var tituloLabel: UILabel!
    @IBOutlet weak var dataLabel: UILabel!
    var content: String?
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setPublicacoesTransparencia(_ publicacoes: PublicacoesTransparencia){
        tituloLabel.text = publicacoes.titulo
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.isLenient = false
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        let dateObj = dateFormatter.date(from: publicacoes.data!)
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dataLabel.text = dateFormatter.string(from: dateObj!)
        let imgURL = URL(string: publicacoes.img!)
        if imgURL != nil {
            img.af_setImage(withURL: imgURL!)
        }
        content = publicacoes.content!
        
        
    }

}
