//
//  Ouvidoria.swift
//  projampa2
//
//  Created by Vinicius Albuquerque on 28/04/17.
//  Copyright © 2017 Vinicius Albuquerque. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import Firebase

class Ouvidoria: NSObject {
    var rua: String?
    var bairro: String?
    var numero: String?
    var mensagem: String?
    
    private var _imgURL: String?
    
    var imgURL: String {
        get {
            return self._imgURL!
        }
        set(newValue) {
            self._imgURL = newValue
        }
    }
    
    private var _img: UIImage?
    
    var img: UIImage {
        get {
            return self._img!
        }
        set(newValue) {
            self._img = newValue
        }
    }
    
    private var _nome: String?
    
    var nome: String {
        get {
            return self._nome!
        }
        set(newValue) {
            self._nome = newValue
    
        }
    }
    
    private var _email: String?
    
    var email: String {
        get {
            return self._email! 
        }
        set (newValue) {
            self._email = newValue
        }
    }
    
    
    
    init(rua: String, bairro: String, numero: String, mensagem: String){
        self.rua = rua
        self.bairro = bairro
        self.numero = numero
        self.mensagem = mensagem
        
    }
    
    func salvarFoto(completionHandler: @escaping (_ retorno: Bool, _ url: String) -> Void) {
        let size = CGSize.init(width: 400, height: 400)
        let newImg = imageResize(image: self._img!, sizeChange: size)
        
        let storage = FIRStorage.storage()
        
        
        let image = NSData(data: UIImageJPEGRepresentation(newImg, 0.0)!)
        let storageref = storage.reference(forURL: "seu firebase storage url")
        let metadata = FIRStorageMetadata()
        metadata.contentType = "image/jpeg"
        let imagesRef = storageref.child("images")
        let number: UInt32 = arc4random_uniform(20) + 10
        let fileName = randomAlphaNumericString(length: Int(number)).md5()
        //let FullRef = "images/\(fileName)"
        let spaceref = imagesRef.child(fileName)
        
        let uploadTask = spaceref.put(image as Data, metadata: metadata)
        
        uploadTask.observe(.success) { snapshot in
            self._imgURL = snapshot.metadata?.downloadURL()?.absoluteString
            let retorno = true
            let url = self._imgURL
            completionHandler(retorno, url!)
            
        }
        uploadTask.observe(.failure) { snapshot in
            let retorno = false
            let url = ""
            completionHandler(retorno, url)
            
        }
        

    }
    
        private func randomAlphaNumericString(length: Int) -> String {
            
            let allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
            let allowedCharsCount = UInt32(allowedChars.characters.count)
            var randomString = ""
            
            for _ in (0..<length) {
                let randomNum = Int(arc4random_uniform(allowedCharsCount))
                let newCharacter = allowedChars[allowedChars.index(allowedChars.startIndex, offsetBy: randomNum)]
                
                randomString += String(newCharacter)
            }
            
            return randomString
        }
        
        private func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
            
            let hasAlpha = true
            let scale: CGFloat = 0.0 // Use o fator de escala necessário obs: se mudar aqui tem que mudar no firebase storage
            
            UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
            image.draw(in: CGRect(origin: .zero, size: sizeChange))
            
            let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
            return scaledImage!
        }
        
        
        
        
        func salvarDados(){
            let url = "sua url para enviar os dados"
            
            let dataCorrente = Date()
            let formatter = DateFormatter()
            formatter.timeStyle = .medium
            formatter.dateStyle = .long
            //formatter.locale = NSLocale.autoupdatingCurrent
            let data = formatter.string(from: dataCorrente)

            let parametros = [
                "rua": self.rua!,
                "bairro": self.bairro!,
                "numero": self.numero!,
                "mensagem": self.mensagem!,
                "imagem": self._imgURL!,
                "nome": self._nome!,
                "email": self._email!,
                "data": data
                
                
                
            ]
            Alamofire.request(url, method: .post, parameters: parametros).responseJSON {response in
                
            }
        }
        
}
