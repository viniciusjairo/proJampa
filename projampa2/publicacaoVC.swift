//
//  publicacaoVC.swift
//  projampa2
//
//  Created by Vinicius Albuquerque on 19/04/17.
//  Copyright © 2017 Vinicius Albuquerque. All rights reserved.
//

import UIKit
import AlamofireImage

class publicacaoVC: UIViewController {
    
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var dataLabel: UILabel!
    
    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var contentView: UIWebView!
    
    var publicacao: PublicacoesTransparencia?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        if let publicacao = publicacao {
            titleLabel.text = publicacao.titulo!
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            dateFormatter.isLenient = false
            dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
            let dateObj = dateFormatter.date(from: publicacao.data!)
            dateFormatter.dateFormat = "dd/MM/yyyy"
            dataLabel.text = dateFormatter.string(from: dateObj!)
            
            let imgURL = URL(string: publicacao.img!)
            if imgURL != nil {
                img.af_setImage(withURL: imgURL!)
            }
            let htmlCode = "<html><header><body>\(publicacao.content!)</body></header></html>"
            contentView.loadHTMLString(htmlCode, baseURL: nil)
            
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
