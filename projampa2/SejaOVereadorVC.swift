//
//  SejaOVereadorVC.swift
//  projampa2
//
//  Created by Vinicius Albuquerque on 10/04/17.
//  Copyright © 2017 Vinicius Albuquerque. All rights reserved.
//

import UIKit
import DLRadioButton
import Alamofire


class SejaOVereadorVC: UIViewController {
    
    @IBOutlet weak var titulo: UILabel!
    
    @IBOutlet weak var contentView: UIWebView!
    
    var SOV: SejaOVereadorModel?
    
    var opiniao: String = ""
    
    var id: Int = 0
    
    var votou: Bool = false
    
    let prefs = UserDefaults.standard
    
    
    
    @IBAction func enviarBTN(_ sender: Any) {
        if(Conexao.isInternetAvailable() == false) {
            let alertController = UIAlertController(title: "Sem Internet", message: "É necessário ter  conexão com a internet", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                
                
            }
            
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        }else {
            if(votou == false){
                if(opiniao == ""){
                    let alertController = UIAlertController(title: "Escolha a Opinião", message: "Escolha SIM ou NÃO", preferredStyle: UIAlertControllerStyle.alert)
                    
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                        
                        
                    }
                    
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }else{
                    SejaOVereadorModel.sendDados(id: self.id, titulo: self.titulo.text!, votacao: self.opiniao)
                    self.prefs.set(true, forKey: "votou")
                    self.votou = true
                    let alertController = UIAlertController(title: "Obrigado", message: "Sua votação foi computada com sucesso", preferredStyle: UIAlertControllerStyle.alert)
                    
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                        self.dismiss(animated: true, completion: nil)
                        
                        
                    }
                    
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }else{
                let alertController = UIAlertController(title: "Você já Votou", message: "Você já votou nessa máteria.", preferredStyle: UIAlertControllerStyle.alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    
                    
                }
                
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let image: UIImage = #imageLiteral(resourceName: "navigationBar")
        self.navigationController?.navigationBar.setBackgroundImage(image, for: .default)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        if(Conexao.isInternetAvailable() == false){
            let alertController = UIAlertController(title: "Sem Internet", message: "É necessário conectar-se a internet", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                self.dismiss(animated: true, completion: nil)
                
            }
            
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }else {
            
            if(self.prefs.value(forKey: "votou") != nil){
                self.votou = self.prefs.bool(forKey: "votou")
            }
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        LoadingOverlay.shared.showOverlay(view: self.view)
        SejaOVereadorModel.getDados() { SOV in
            self.titulo.text = SOV.titulo!
            
            self.id = SOV.id!
            if(self.prefs.value(forKey: "id_SOV") != nil){
                let id = self.prefs.string(forKey: "id_SOV")
                if(id != "\(self.id)"){
                    self.prefs.set(false, forKey: "votou")
                    self.votou = false
                }
            }
            let htmlCode = "<html><header><body>\(SOV.conteudo!)</body></header></html>"
            self.contentView.loadHTMLString(htmlCode, baseURL: nil)
            self.prefs.setValue("\(SOV.id!)", forKey: "id_SOV")
            
        }
        LoadingOverlay.shared.hideOverlayView()
        
    }
    
    @IBAction func voltarBTN(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func simBTN(_ sender: Any) {
        
        opiniao = "Projeto 1"
    }
    
    @IBAction func naoBTN(_ sender: Any) {
        opiniao = "Projeto 2"
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
