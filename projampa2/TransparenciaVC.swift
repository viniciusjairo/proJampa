//
//  TransparenciaVC.swift
//  projampa2
//
//  Created by Vinicius Albuquerque on 11/04/17.
//  Copyright © 2017 Vinicius Albuquerque. All rights reserved.
//

import UIKit

class TransparenciaVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var navigationBar: UINavigationItem!
    var publicacoes: [PublicacoesTransparencia]?
    
    @IBAction func voltarBTN(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let image: UIImage = #imageLiteral(resourceName: "navigationBar")
        self.navigationController?.navigationBar.setBackgroundImage(image, for: .default)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        tableView.dataSource = self
        tableView.delegate = self
        tableView.contentInset = UIEdgeInsetsMake(5, 0, 5, 0)
        
        if(Conexao.isInternetAvailable() == false){
            let alertController = UIAlertController(title: "Sem Internet", message: "É necessário conectar-se a internet", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                self.dismiss(animated: true, completion: nil)
                
            }
            
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }else {
            
            PublicacoesTransparencia.getPublicacoes() { (array) in
                self.publicacoes = array
                self.tableView.reloadData()
            }
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension TransparenciaVC: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if publicacoes != nil {
            
            return publicacoes!.count
            
        } else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PubTransCell") as! PubTransCell
        
        cell.setPublicacoesTransparencia(publicacoes![indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        self.performSegue(withIdentifier: "publicacao", sender: publicacoes?[indexPath.row])
        
        
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "publicacao"){
            let publicacaoVC: publicacaoVC = segue.destination as! publicacaoVC
            let publicacao = sender as! PublicacoesTransparencia
            publicacaoVC.publicacao = publicacao
        }
    }
    
    
    
    
}
