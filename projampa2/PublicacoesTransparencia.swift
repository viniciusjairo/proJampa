//
//  transparenciaModel.swift
//  projampa2
//
//  Created by Vinicius Albuquerque on 18/04/17.
//  Copyright © 2017 Vinicius Albuquerque. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class PublicacoesTransparencia: NSObject {
    
    var id: Int?
    var titulo: String?
    var data: String?
    var img: String?
    var content: String?
    
    init(id: Int, titulo: String, data: String, img: String, content: String) {
        self.id = id
        self.titulo = titulo
        self.data = data
        self.img = img
        self.content = content
    }
    
    class func getPublicacoes(completionHandler: @escaping (_ publicacoes: [PublicacoesTransparencia]) -> Void){
        let url = "http://projampathiago.hospedagemdesites.ws/wp-json/wp/v2/posts?categories=3"
        
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default).responseJSON { response in
            let json = JSON(data: response.data!)
            var publicacoesArray = [PublicacoesTransparencia]()
            for(_, subJson):(String, JSON) in json{
                let PubTrans = PublicacoesTransparencia.init(id: subJson["id"].intValue,
                                                             titulo: subJson["title"]["rendered"].stringValue,
                                                             data: subJson["modified"].stringValue,
                
                                                             img: subJson["better_featured_image"]["media_details"]["sizes"]["medium"]["source_url"].stringValue,
                                                             content: subJson["content"]["rendered"].stringValue)
                publicacoesArray.append(PubTrans)
                
            }
            
            completionHandler(publicacoesArray)
            
        }
        
        
        
        
    }
    
    
    
    

}
