//
//  EnviarVC.swift
//  projampa2
//
//  Created by Vinicius Albuquerque on 28/04/17.
//  Copyright © 2017 Vinicius Albuquerque. All rights reserved.
//

import UIKit

class EnviarVC: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var nomeField: UITextField!
    
    @IBOutlet weak var emailField: UITextField!
    
   
    @IBOutlet weak var scrollView: UIScrollView!
    
    var ouvidoria: Ouvidoria?

    var activeField: UITextField?

    
    @IBAction func enviarBTN(_ sender: Any) {
        if(nomeField.text == ""){
            ouvidoria?.nome = "anônimo"
        }
        if(emailField.text == ""){
            ouvidoria?.email = "sem email"
        }
        ouvidoria?.nome = nomeField.text!
        ouvidoria?.email = emailField.text!
        
        if(Conexao.isInternetAvailable()){
            ouvidoria?.salvarDados()
            let alertController = UIAlertController(title: "Dados Salvo", message: "Sua mensagem foi enviada com sucesso!", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                print("OK")
                self.dismiss(animated: true, completion: nil)
                
            }
            
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }else{
            let alertController = UIAlertController(title: "Erro", message: "Sem conexão com a internet", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                print("OK")
                
            }
            
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
        
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let image: UIImage = #imageLiteral(resourceName: "navigationBar")
        self.navigationController?.navigationBar.setBackgroundImage(image, for: .default)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        self.nomeField.delegate = self
        self.emailField.delegate = self


        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ColaboreVC.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        registerForKeyboardNotifications()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
         deregisterFromKeyboardNotifications()
        
    }


    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(ouvidoria?.img != nil ){
            img.image = ouvidoria?.img
        }
        
    }
    
    func dismissKeyboard(){
        //textField.resignFirstResponder()
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.nomeField.resignFirstResponder()
        self.emailField.resignFirstResponder()
        
        return true
    }
    
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.isScrollEnabled = true
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeField {
            if (!aRect.contains(activeField.frame.origin)){
                self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions

    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
